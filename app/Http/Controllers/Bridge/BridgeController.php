<?php

namespace App\Http\Controllers\Bridge;

use App\Http\Controllers\Bridge\Theme\Object\DarkTheme;
use App\Http\Controllers\Bridge\WebPagees\Object\About;
use App\Http\Controllers\Bridge\WebPagees\Object\Careers;

class BridgeController
{
    public function Attribute(){
        $darkTheme = new DarkTheme();

        $about = new About($darkTheme);
        $careers = new Careers($darkTheme);

        echo $about->getContent(); // "Trang giới thiệu có màu Đen";
        echo '</br>';
        echo $careers->getContent(); // "Trang tuyển dụng màu đen";
    }
}
