<?php

namespace App\Http\Controllers\Bridge\WebPagees\Object;

use App\Http\Controllers\Bridge\WebPagees\Interfaces\WebPage;
use App\Http\Controllers\Bridge\Theme\Interfaces\Theme;

class Careers implements WebPage
{
    protected $theme;

    public function __construct(Theme $theme)
    {
        $this->theme = $theme;
    }

    public function getContent()
    {
        return "Careers page in " . $this->theme->getColor();
    }
}
