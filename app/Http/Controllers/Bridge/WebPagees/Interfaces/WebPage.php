<?php

namespace App\Http\Controllers\Bridge\WebPagees\Interfaces;

use App\Http\Controllers\Bridge\Theme\Interfaces\Theme;

interface WebPage
{
    public function __construct(Theme $theme);
    public function getContent();
}
