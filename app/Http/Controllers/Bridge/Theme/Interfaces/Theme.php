<?php

namespace App\Http\Controllers\Bridge\Theme\Interfaces;

interface Theme
{
    public function getColor();
}
