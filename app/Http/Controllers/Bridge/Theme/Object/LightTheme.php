<?php

namespace App\Http\Controllers\Bridge\Theme\Object;

use App\Http\Controllers\Bridge\Theme\Interfaces\Theme;

class LightTheme implements Theme
{
    public function getColor()
    {
        return 'Off white';
    }
}
