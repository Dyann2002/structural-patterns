<?php

namespace App\Http\Controllers\Bridge\Theme\Object;

use App\Http\Controllers\Bridge\Theme\Interfaces\Theme;

class AquaTheme implements Theme
{
    public function getColor()
    {
        return 'Light blue';
    }
}
