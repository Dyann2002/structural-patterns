<?php

namespace App\Http\Controllers\Bridge\Theme\Object;

use App\Http\Controllers\Bridge\Theme\Interfaces\Theme;

class DarkTheme implements Theme
{
    public function getColor()
    {
        return 'Dark Black';
    }
}
