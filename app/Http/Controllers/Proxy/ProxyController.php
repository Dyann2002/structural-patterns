<?php

namespace App\Http\Controllers\Proxy;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Proxy\Door\SecuredDoor;
use App\Http\Controllers\Proxy\Door\Object\LabDoor;

class ProxyController extends Controller
{
    public function OpenDoor(){
        $door = new SecuredDoor(new LabDoor());
        $door->open('invalid'); // Big no! It ain't possible.

        $door->open('$ecr@t'); // Opening lab door
    }
}
