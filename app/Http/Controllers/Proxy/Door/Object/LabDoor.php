<?php

namespace App\Http\Controllers\Proxy\Door\Object;

use App\Http\Controllers\Proxy\Door\Interfaces\Door;

class LabDoor implements Door
{
    public function open()
    {
        echo "Opening lab door";
    }
}
