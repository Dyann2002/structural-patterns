<?php

namespace App\Http\Controllers\Proxy\Door;

use App\Http\Controllers\Proxy\Door\Interfaces\Door;

class SecuredDoor
{
    protected $door;

    public function __construct(Door $door)
    {
        $this->door = $door;
    }

    public function authenticate($password)
    {
        return $password === '$ecr@t';
    }

    public function open($password)
    {
        if ($this->authenticate($password)) {
            $this->door->open();
        } else {
            echo "Big no! It ain't possible.";
        }
    }
}
