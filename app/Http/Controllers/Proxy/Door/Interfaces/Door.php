<?php

namespace App\Http\Controllers\Proxy\Door\Interfaces;

interface Door
{
    public function open();
}
