<?php

namespace App\Http\Controllers\Adapter\Lion\Object;

use App\Http\Controllers\Adapter\Lion\Interfaces\Lion;

class AsianLion implements Lion
{
    public function roar()
    {
        return 'AsianLion';
    }
}
