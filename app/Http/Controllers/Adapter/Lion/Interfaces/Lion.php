<?php

namespace App\Http\Controllers\Adapter\Lion\Interfaces;

interface Lion
{
    public function roar();
}
