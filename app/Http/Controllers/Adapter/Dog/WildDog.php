<?php

namespace App\Http\Controllers\Adapter\Dog;

class WildDog
{
    public function bark()
    {
        return 'WildDog';
    }
}
