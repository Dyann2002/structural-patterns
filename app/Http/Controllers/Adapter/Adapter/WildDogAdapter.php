<?php

namespace App\Http\Controllers\Adapter\Adapter;

use App\Http\Controllers\Adapter\Lion\Interfaces\Lion;
use App\Http\Controllers\Adapter\Dog\WildDog;

class WildDogAdapter implements Lion
{
    protected $dog;

    public function __construct(WildDog $dog)
    {
        $this->dog = $dog;
    }

    public function roar()
    {
        return $this->dog->bark();
    }
}
