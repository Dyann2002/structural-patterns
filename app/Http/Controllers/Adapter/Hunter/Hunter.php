<?php

namespace App\Http\Controllers\Adapter\Hunter;

use App\Http\Controllers\Adapter\Lion\Interfaces\Lion;

class Hunter
{
    public function hunt(Lion $lion)
    {
        return $lion->roar();
    }
}
