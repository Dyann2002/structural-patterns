<?php

namespace App\Http\Controllers\Adapter;

use App\Http\Controllers\Adapter\Dog\WildDog;
use App\Http\Controllers\Adapter\Hunter\Hunter;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Adapter\Adapter\WildDogAdapter;

class AdapterController extends Controller
{
    public function huntAnimal(){
        $wildDogAdapter = new WildDogAdapter(new WildDog());

        $hunter = new Hunter();
        return $hunter->hunt($wildDogAdapter);
    }
}
