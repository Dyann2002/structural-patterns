<?php

namespace App\Http\Controllers\Facade;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Facade\Computer\ComputerFacade;
use App\Http\Controllers\Facade\Computer\Object\Computer;

class FacadeController extends Controller
{
    public function Open(){
        $computer = new ComputerFacade(new Computer());
        $computer->turnOn(); // Ouch! Beep beep! Loading.. Ready to be used!
    }
}
