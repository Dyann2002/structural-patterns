<?php

namespace App\Http\Controllers\Facade\Computer\Object;

class Computer
{
    public function powerIn()
    {
        echo "Beep beep!";
    }

    public function showLoadingScreen()
    {
        echo "Loading..";
    }

    public function bam()
    {
        echo "Ready to be used!";
    }
}
