<?php

namespace App\Http\Controllers\Facade\Computer;

use App\Http\Controllers\Facade\Computer\Object\Computer;

class ComputerFacade
{
    protected $computer;

    public function __construct(Computer $computer)
    {
        $this->computer = $computer;
    }

    public function turnOn()
    {
        $this->computer->powerIn();
        $this->computer->showLoadingScreen();
        $this->computer->bam();
    }
}
