<?php

namespace App\Http\Controllers\Flyweight;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flyweight\Tea\Object\TeaMaker;
use App\Http\Controllers\Flyweight\Tea\Object\TeaShop;

class FlyweightController extends Controller
{
    public function ListOrder(){
        $teaMaker = new TeaMaker();
        $shop = new TeaShop($teaMaker);

        $shop->takeOrder('less sugar', 1);
        $shop->takeOrder('more milk', 2);
        $shop->takeOrder('without sugar', 5);

        $shop->serve();
        // Serving tea to table# 1
        // Serving tea to table# 2
        // Serving tea to table# 5
    }
}
