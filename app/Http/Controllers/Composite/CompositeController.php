<?php

namespace App\Http\Controllers\Composite;

use App\Http\Controllers\Composite\Employee\Object\Developer;
use App\Http\Controllers\Composite\Employee\Object\Designer;
use App\Http\Controllers\Composite\Organization\Organization;
use App\Http\Controllers\Controller;

class CompositeController extends Controller
{
    public function Organization(){
        // Prepare the employees
        $john = new Developer('John Doe', 12000);
        $jane = new Designer('Jane Doe', 15000);

        // Add them to organization
        $organization = new Organization();
        $organization->addEmployee($john);
        $organization->addEmployee($jane);

        echo "Net salaries: " . $organization->getNetSalaries(); // Net Salaries: 27000

    }
}
