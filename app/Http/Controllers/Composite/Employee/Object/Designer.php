<?php

namespace App\Http\Controllers\Composite\Employee\Object;

use App\Http\Controllers\Composite\Employee\Interfaces\Employee;

class Designer implements Employee
{
    protected $salary;
    protected $name;

    public function __construct(string $name, float $salary)
    {
        $this->name = $name;
        $this->salary = $salary;
    }

    public function getSalary(): float
    {
        return $this->salary;
    }
}
