<?php

namespace App\Http\Controllers\Composite\Employee\Interfaces;

interface Employee
{
    public function __construct(string $name, float $salary);
    public function getSalary(): float;
}
