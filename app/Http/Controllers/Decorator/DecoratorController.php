<?php

namespace App\Http\Controllers\Decorator;

use App\Http\Controllers\Decorator\Coffee\Object\SimpleCoffee;
use App\Http\Controllers\Decorator\Coffee\Object\MilkCoffee;
use App\Http\Controllers\Controller;

class DecoratorController extends Controller
{
    public function Menu(){
        $someCoffee = new SimpleCoffee();
        echo $someCoffee->getCost(); // 10
        echo $someCoffee->getDescription(); // Simple Coffee
        echo '</br>';

        $someCoffee = new MilkCoffee($someCoffee);
        echo $someCoffee->getCost(); // 12
        echo $someCoffee->getDescription(); // Simple Coffee, milk

    }
}
