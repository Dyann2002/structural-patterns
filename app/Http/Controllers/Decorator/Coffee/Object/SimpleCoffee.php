<?php

namespace App\Http\Controllers\Decorator\Coffee\Object;

use App\Http\Controllers\Decorator\Coffee\Interfaces\Coffee;

class SimpleCoffee implements Coffee
{
    public function getCost()
    {
        return 10;
    }

    public function getDescription()
    {
        return 'Simple coffee';
    }
}
