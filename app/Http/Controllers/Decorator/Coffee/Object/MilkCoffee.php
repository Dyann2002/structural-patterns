<?php

namespace App\Http\Controllers\Decorator\Coffee\Object;

use App\Http\Controllers\Decorator\Coffee\Interfaces\Coffee;

class MilkCoffee implements Coffee
{
    protected $coffee;

    public function __construct(Coffee $coffee)
    {
        $this->coffee = $coffee;
    }

    public function getCost()
    {
        return $this->coffee->getCost() + 2;
    }

    public function getDescription()
    {
        return $this->coffee->getDescription() . ', milk';
    }
}

