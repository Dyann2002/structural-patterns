<?php

namespace App\Http\Controllers\Decorator\Coffee\Interfaces;

interface Coffee
{
    public function getCost();
    public function getDescription();
}

