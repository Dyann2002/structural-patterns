<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
], function () {
    Route::get('adapter', 'App\Http\Controllers\Adapter\AdapterController@huntAnimal');
    Route::get('bridge', 'App\Http\Controllers\Bridge\BridgeController@Attribute');
    Route::get('composite', 'App\Http\Controllers\Composite\CompositeController@Organization');
    Route::get('decorator', 'App\Http\Controllers\Decorator\DecoratorController@Menu');
    Route::get('facade', 'App\Http\Controllers\Facade\FacadeController@Open');
    Route::get('flyweight', 'App\Http\Controllers\Flyweight\FlyweightController@ListOrder');
    Route::get('proxy', 'App\Http\Controllers\Proxy\ProxyController@OpenDoor');
});
